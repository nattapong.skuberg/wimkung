const mongoose = require('mongoose');
const findOrCreate = require('findorcreate-promise');

let Schema = mongoose.Schema;

let UserSchema = new Schema({
    facebook_id: String,
    facebook_state: [String]
});

UserSchema.plugin(findOrCreate);

let User = mongoose.model('User', UserSchema);

module.exports = User;