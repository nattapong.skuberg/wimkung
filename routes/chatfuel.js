const express = require('express');
const router = express.Router();
const currencyController = require('../controllers/currencyController');

router.post('/lastPrice', currencyController.getPrice);

module.exports = router;
