const express = require('express');
const router = express.Router();
const facebookController = require('../controllers/facebookController');

router.get('/webhook', facebookController.getWebHook);
router.post('/webhook', facebookController.postWebHook);
router.get('/authorize', facebookController.authorize);


module.exports = router;
