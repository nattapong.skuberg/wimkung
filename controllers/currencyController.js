const axios = require('axios');
const _ = require('lodash');

exports.getPrice = async function (req, res){
    console.log(req.body);
    let pair = req.body.pair;
    let result = await getPriceBinance(pair);
    if (result == null) {
        let data = {
            'messages' : [{"text" : 'Currency pair invalid.'}]
        };
        res.json(data);
    }
    let data = {
        'messages': [{"text" : result}]
    };
    res.json(data);
};

async function getPriceBinance(pair) {
    try {
        let response = await axios.get('https://api.binance.com/api/v1/ticker/24hr?symbol=' + pair);
        let data = response.data;
        data = _.pick(data, ['symbol', 'priceChangePercent', 'lastPrice', 'bidPrice', 'askPrice', 'volume']);
        let text = '';
        for (const key in data){
            text = text + key + ' : ' + data[key] + ' \n';
        }
        return text;
    }
    catch (error){
        console.log(error.response.data.msg);
        return null;
    }
}