const config = require('config');
const request = require('request');
const facebookService = require('../services/facebookService');
const User = require('../models/User');
const axios = require('axios');
const _ = require('lodash');

// App Secret can be retrieved from the App Dashboard
const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ? process.env.MESSENGER_APP_SECRET : config.get('appSecret');

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = (process.env.MESSENGER_VALIDATION_TOKEN) ? (process.env.MESSENGER_VALIDATION_TOKEN) : config.get('validationToken');

// Generate a page access token for your page from the App Dashboard
const PAGE_ACCESS_TOKEN = (process.env.MESSENGER_PAGE_ACCESS_TOKEN) ? (process.env.MESSENGER_PAGE_ACCESS_TOKEN) : config.get('pageAccessToken');

// URL where the app is running (include protocol). Used to point to scripts and
// assets located at this address.
const SERVER_URL = (process.env.SERVER_URL) ? (process.env.SERVER_URL) : config.get('serverURL');

if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN && SERVER_URL)) {
    console.error("Missing config values");
    process.exit(1);
}

exports.getWebHook = function (req, res) {
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === VALIDATION_TOKEN) {
        console.log("Validating webhook");
        res.status(200).send(req.query['hub.challenge']);
    } else {
        console.error("Failed validation. Make sure the validation tokens match.");
        res.sendStatus(403);
    }
};
exports.postWebHook = function (req, res) {
    let data = req.body;

    // Make sure this is a page subscription
    if (data.object == 'page') {
        // Iterate over each entry
        // There may be multiple if batched
        data.entry.forEach(function(pageEntry) {
            let pageID = pageEntry.id;
            let timeOfEvent = pageEntry.time;

            // Iterate over each messaging event
            pageEntry.messaging.forEach(function(messagingEvent) {
                if (messagingEvent.optin) {
                    receivedAuthentication(messagingEvent);
                } else if (messagingEvent.message) {
                    try {
                        receivedMessage(messagingEvent);
                    }
                    catch (err){
                        console.log(err);
                    }
                } else if (messagingEvent.delivery) {
                    receivedDeliveryConfirmation(messagingEvent);
                } else if (messagingEvent.postback) {
                    try {
                        receivedPostback(messagingEvent);
                    }
                    catch (err){
                        console.log(err);
                    }
                } else if (messagingEvent.read) {
                    receivedMessageRead(messagingEvent);
                } else if (messagingEvent.account_linking) {
                    receivedAccountLink(messagingEvent);
                } else {
                    console.log("Webhook received unknown messagingEvent: ", messagingEvent);
                }
                console.log("Webhook received messagingEvent: ", messagingEvent);
            });
        });

        // Assume all went well.
        //
        // You must send back a 200, within 20 seconds, to let us know you've
        // successfully received the callback. Otherwise, the request will time out.
        res.sendStatus(200);
    }
};
exports.authorize = function (req, res) {
    let accountLinkingToken = req.query.account_linking_token;
    let redirectURI = req.query.redirect_uri;

    // Authorization Code should be generated per user by the developer. This will
    // be passed to the Account Linking callback.
    let authCode = "1234567890";

    // Redirect users to this URI on successful login
    let redirectURISuccess = redirectURI + "&authorization_code=" + authCode;

    res.render('authorize', {
        accountLinkingToken: accountLinkingToken,
        redirectURI: redirectURI,
        redirectURISuccess: redirectURISuccess
    });
};


/*
 * Verify that the callback came from Facebook. Using the App Secret from
 * the App Dashboard, we can verify the signature that is sent with each
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
function verifyRequestSignature(req, res, buf) {
    let signature = req.headers["x-hub-signature"];

    if (!signature) {
        // For testing, let's log an error. In production, you should throw an
        // error.
        console.error("Couldn't validate the signature.");
    } else {
        let elements = signature.split('=');
        let method = elements[0];
        let signatureHash = elements[1];

        let expectedHash = crypto.createHmac('sha1', APP_SECRET)
            .update(buf)
            .digest('hex');

        if (signatureHash != expectedHash) {
            throw new Error("Couldn't validate the request signature.");
        }
    }
}

/*
 * Authorization Event
 *
 * The value for 'optin.ref' is defined in the entry point. For the "Send to
 * Messenger" plugin, it is the 'data-ref' field. Read more at
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/authentication
 *
 */
function receivedAuthentication(event) {
    let senderID = event.sender.id;
    let recipientID = event.recipient.id;
    let timeOfAuth = event.timestamp;

    // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
    // The developer can set this to an arbitrary value to associate the
    // authentication callback with the 'Send to Messenger' click event. This is
    // a way to do account linking when the user clicks the 'Send to Messenger'
    // plugin.
    let passThroughParam = event.optin.ref;

    console.log("Received authentication for user %d and page %d with pass " +
        "through param '%s' at %d", senderID, recipientID, passThroughParam,
        timeOfAuth);

    // When an authentication is received, we'll send a message back to the sender
    // to let them know it was successful.
    facebookService.sendTextMessage(senderID, "Authentication successful");
}

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message'
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-received
 *
 * For this example, we're going to echo any text that we get. If we get some
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've
 * created. If we receive a message with an attachment (image, video, audio),
 * then we'll simply confirm that we've received the attachment.
 *
 */
async function receivedMessage(event) {
    let senderID = event.sender.id;
    let recipientID = event.recipient.id;
    let timeOfMessage = event.timestamp;
    let message = event.message;

    console.log("Received message for user %d and page %d at %d with message:",
        senderID, recipientID, timeOfMessage);
    console.log(JSON.stringify(message));

    let isEcho = message.is_echo;
    let messageId = message.mid;
    let appId = message.app_id;
    let metadata = message.metadata;

    // You may get a text or attachment but not both
    let messageText = message.text;
    let messageAttachments = message.attachments;
    let quickReply = message.quick_reply;

    facebookService.sendTypingOn(senderID);

    if (isEcho) {
        // Just logging message echoes to console
        console.log("Received echo for message %s and app %d with metadata %s",
            messageId, appId, metadata);
        return;
    } else if (quickReply) {
        let quickReplyPayload = quickReply.payload;
        console.log("Quick reply for message %s with payload %s",
            messageId, quickReplyPayload);

        facebookService.sendTextMessage(senderID, "Quick reply tapped");
        return;
    }


    if (messageText) {
        let message = messageText.replace(/[^\w\s]/gi, '').trim().toLowerCase();
        await pushStateUser(senderID, message);
        switch (message) {
            case 'home':
            case 'hello':
            case 'hi':
                await clearState(senderID);

                facebookService.sendTextMessage(senderID, 'Hello! Home.');
                facebookService.sendHiMessage(senderID);

                let title = "Select Mode";
                let payloads =[{
                    type: "postback",
                    title: "Crypto",
                    payload: "crypto"
                }, {
                    type: "web_url",
                    url: "https://www.gratiprekkaeng.com",
                    title: "Website"
                }];

                facebookService.sendButtonMessage(senderID, title, payloads);

                break;
            case 'image':
                requiresServerURL(facebookService.sendImageMessage, [senderID]);
                break;

            case 'gif':
                requiresServerURL(facebookService.sendGifMessage, [senderID]);
                break;

            case 'audio':
                requiresServerURL(facebookService.sendAudioMessage, [senderID]);
                break;

            case 'video':
                requiresServerURL(facebookService.sendVideoMessage, [senderID]);
                break;

            case 'file':
                requiresServerURL(facebookService.sendFileMessage, [senderID]);
                break;

            case 'generic':
                requiresServerURL(facebookService.sendGenericMessage, [senderID]);
                break;

            case 'receipt':
                requiresServerURL(facebookService.sendReceiptMessage, [senderID]);
                break;

            case 'quick reply':
                facebookService.sendQuickReply(senderID);
                break;

            case 'read receipt':
                facebookService.sendReadReceipt(senderID);
                break;
            case 'account linking':
                requiresServerURL(facebookService.sendAccountLinking, [senderID]);
                break;
        }
    } else if (messageAttachments) {
        facebookService.sendTextMessage(senderID, "Message with attachment received");
    }

    let facebook_state =  await getStateUser(senderID);
    if(facebook_state.length > 0 && facebook_state[0] === 'crypto'){
        switch (facebook_state[1]){
            case 'lastPrice':
                let pair = messageText.replace(/[^\w\s]/gi, '').trim().toUpperCase();

                try {
                    let response = await axios.get('https://api.binance.com/api/v1/ticker/24hr?symbol=' + pair);
                    let data = response.data;
                    data = _.pick(data, ['symbol', 'priceChangePercent', 'lastPrice', 'bidPrice', 'askPrice', 'volume']);
                    let text = '';
                    for (const key in data){
                        text = text + key + ' : ' + data[key] + ' \n';
                    }
                    facebookService.sendTextMessage(senderID, text);
                }
                catch (error){
                    console.log(error.response.data.msg);
                    facebookService.sendTextMessage(senderID, error.response.data.msg);
                }

                break;
            case 'observe':

                break;
        }
    }


    facebookService.sendTypingOff(senderID);
}


/*
 * Delivery Confirmation Event
 *
 * This event is sent to confirm the delivery of a message. Read more about
 * these fields at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-delivered
 *
 */
function receivedDeliveryConfirmation(event) {
    let senderID = event.sender.id;
    let recipientID = event.recipient.id;
    let delivery = event.delivery;
    let messageIDs = delivery.mids;
    let watermark = delivery.watermark;
    let sequenceNumber = delivery.seq;

    if (messageIDs) {
        messageIDs.forEach(function(messageID) {
            console.log("Received delivery confirmation for message ID: %s",
                messageID);
        });
    }

    console.log("All message before %d were delivered.", watermark);
}


/*
 * Postback Event
 *
 * This event is called when a postback is tapped on a Structured Message.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/postback-received
 *
 */
async function receivedPostback(event) {
    let senderID = event.sender.id;
    let recipientID = event.recipient.id;
    let timeOfPostback = event.timestamp;

    // The 'payload' param is a developer-defined field which is set in a postback
    // button for Structured Messages.
    let payload = event.postback.payload;

    facebookService.sendTypingOn(senderID);

    console.log("Received postback for user %d and page %d with payload '%s' " +
        "at %d", senderID, recipientID, payload, timeOfPostback);

    if (payload){
        let message = payload.replace(/[^\w\s]/gi, '').trim();
        let facebook_state =  await getStateUser(senderID);
        switch (message){
            case 'crypto':
                await pushStateUser(senderID, message);
                let title = 'Cypto mode';
                let buttons = [{
                    type: "postback",
                    title: "Last price",
                    payload: "lastPrice"
                },{
                    type: "postback",
                    title: "Observe Address",
                    payload: "observe"
                }];
                facebookService.sendButtonMessage(senderID,title, buttons)
                break;

            case 'lastPrice':
                if (facebook_state.length > 0 && facebook_state[0] === 'crypto'){
                    await pushStateUser(senderID, message);
                    facebookService.sendTextMessage(senderID, 'Enter ticker (Ex. BTC/USDT)');
                }
                break;

            case 'observe':
                if (facebook_state.length > 0 && facebook_state[0] === 'crypto'){
                    await pushStateUser(senderID, message);
                    facebookService.sendTextMessage(senderID, 'Last price mode');
                }
                break;
        }
    }
    else {
        facebookService.sendTextMessage(senderID, "Postback called");
    }
    facebookService.sendTypingOff(senderID);
}

/*
 * Message Read Event
 *
 * This event is called when a previously-sent message has been read.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-read
 *
 */
function receivedMessageRead(event) {
    let senderID = event.sender.id;
    let recipientID = event.recipient.id;

    // All messages before watermark (a timestamp) or sequence have been seen.
    let watermark = event.read.watermark;
    let sequenceNumber = event.read.seq;

    console.log("Received message read event for watermark %d and sequence " +
        "number %d", watermark, sequenceNumber);
}

/*
 * Account Link Event
 *
 * This event is called when the Link Account or UnLink Account action has been
 * tapped.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/account-linking
 *
 */
function receivedAccountLink(event) {
    let senderID = event.sender.id;
    let recipientID = event.recipient.id;

    let status = event.account_linking.status;
    let authCode = event.account_linking.authorization_code;

    console.log("Received account link event with for user %d with status %s " +
        "and auth code %s ", senderID, status, authCode);
}

function requiresServerURL(next, [recipientId, ...args]) {
    next.apply(this, [recipientId, ...args]);
}

async function pushStateUser(sendId, facebook_state) {
    let result = await User.findOrCreate({facebook_id:sendId});
    let user = result.result;
    let message = user.facebook_state;
    if (message.length >= 5){
        message.shift();
    }
    message.push(facebook_state);
    user.set({facebook_state:message});
    await user.save();
}

async function clearState(sendId) {
    let result = await User.findOrCreate({facebook_id:sendId});
    let user = result.result;

    user.set({facebook_state:[]});
    await user.save();
}

async function getStateUser(sendId) {
    let result = await User.findOrCreate({facebook_id:sendId});
    let user = result.result;

    return user.facebook_state;
}